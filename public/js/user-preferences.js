var availableFoodList = {
	'breakfast': [],
	'lunch': [],
	'snacks': [],
	'others': [],
};

window.onload = function() {
	fetch('http://localhost:3000/get_food_items')
	.then(response => response.json())
	.then(response => {
		if(response.status == 200) {
			console.log(response.data);
			response.data.map((record) => {
				if(record.name == "BREAKFAST") {
					availableFoodList.breakfast.push(record);
				} else if(record.name == "SNACKS") {
					availableFoodList.snacks.push(record);
				} else if(record.name == "OTHERS (salad+curd+buttermilk+papad+pickle/chatni)" || record.name == "SWEETS") {
					availableFoodList.others.push(record);
				} else {
					availableFoodList.lunch.push(record);
				}
			});
			preferenceMethods.createUIOptions(availableFoodList);
		}
	});
};

let preferenceMethods = {
	preferenceStates: ['breakfast', 'lunch', 'snacks', 'others'],
	setActive: function(event) {
		console.log("inside set active");
		let key = event.currentTarget.id;
		for(let i = 0; i < this.preferenceStates.length; i++) {
			if(this.preferenceStates[i] == key) {
				document.getElementById(key).classList.add('is-active');
				$('#'+key+'-items').css("display", "block");
			} else {
				if(document.getElementById(this.preferenceStates[i]).classList.contains('is-active')) {
					document.getElementById(this.preferenceStates[i]).classList.remove('is-active');
					$('#'+this.preferenceStates[i]+'-items').css("display", "none");
				}
			}
		}
	},
	setActiveItem: function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		console.log(event.target.id);
		if($('#'+event.target.id).hasClass('is-primary')) {
			$('#'+event.target.id).removeClass('is-primary');
		} else {
			$('#'+event.target.id).addClass('is-primary');
		}
	},
	createUIOptions: function(availableFoodList) {
		let breakfastElements = '';
		let lunchElements = '';
		let snackElements = '';
		let otherElements = '';
		for(let key in availableFoodList) {
			if(key == 'breakfast') {
				availableFoodList[key].map((record) => {
					breakfastElements += `<button class="button m-2" onclick="preferenceMethods.setActiveItem(event)" id="${record.id}">${record.item_name}</button>`;
				});
			}
			if(key == 'lunch') {
				availableFoodList[key].map((record) => {
					lunchElements += `<button class="button m-2" onclick="preferenceMethods.setActiveItem(event)" id="${record.id}">${record.item_name}</button>`;
				});
			}
			if(key == 'snacks') {
				availableFoodList[key].map((record) => {
					snackElements += `<button class="button m-2" onclick="preferenceMethods.setActiveItem(event)" id="${record.id}">${record.item_name}</button>`;
				});	
			}
			if(key == 'others') {
				availableFoodList[key].map((record) => {
					otherElements += `<button class="button m-2" onclick="preferenceMethods.setActiveItem(event)" id="${record.id}">${record.item_name}</button>`;
				});
			}
		}
		$('#breakfast-items').append(breakfastElements);
		$('#lunch-items').append(lunchElements);
		$('#snacks-items').append(snackElements);
		$('#others-items').append(otherElements);
	},
	postPreferenceForm: function(event) {
		console.log(event);
		event.stopImmediatePropagation();
		event.preventDefault();
		let formdata = {};
		formdata['username'] = $('#username').val();
		formdata['veg'] = $("#veg").is(':checked');
		formdata['non_veg'] = $("#non-veg").is(':checked');
		formdata['items'] = [];
		let preferredItems = $('.is-primary').map(function(elem) {
			formdata['items'].push(this.id);
		});
		console.log(formdata);
		$.ajax({
			type: 'POST',
			url: 'http://localhost:3000/save_user_preference', 
			data: JSON.stringify(formdata),
			// dataType: 'json',
			contentType: 'application/json',
			success: function(data) {
				console.log(data);
			}
		});
	}
}