const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
router = express.Router();
const db = require('./model/db');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});

app.get('/get_food_items', function(req, res) {
  console.log("in");
  let query = 'Select food_item.id, food_item.item_name, categories.name from food_item inner join categories on food_item.category_id = categories.id';
  db.query(query, function(err, data, fields) {
    if(err) throw err;
    res.json({
      status: 200,
      data,
      message: "List Received",
    });
  });
});

app.post('/save_user_preference', function(req, res) {
  // console.log(req.body);
  let parsedData = req.body;
  let dataToSave = '';
  let pref = 'Both';
  if(parsedData.veg == true) {
    pref = 'Veg';
  } else if(parsedData.non_veg == true) {
    pref = 'Both';
  }
  parsedData.items.map(function(record) {
    dataToSave += `(${record},'${parsedData.username}',1,'${pref}','user-preference'),`;
  });
  dataToSave = dataToSave.substring(0, dataToSave.length - 1);
  console.log(dataToSave);
  let query = 'INSERT INTO users_review (item_id,reviewer_name,rating,food_preference,source ) VALUES ' + dataToSave;
  console.log(query);
  db.query(query, function(err, data, fields) {
    if(err) throw err;
    res.json({
      status: 200,
      message: "Preference submitted!!",
    });
  });
});